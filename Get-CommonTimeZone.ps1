Function Get-CommonTimeZone{
   
    <#
        .SYNOPSIS
        Retrieve Time zone data based on Name or Offset value.

        .DESCRIPTION
        Fetches timezone details information from Json file located on Git
        Returns filtered data based on supplied parameter values

        .PARAMETER Name
        Specifies the string to filter timezone name that includes specified string

        .PARAMETER Offset
        Specifies the offet value to filter data based on offset

        .OUTPUTS
        System.Array Returns respective time zone data based on specified input.

        .EXAMPLE
        PS> Get-CommonTimeZone -Name "Asia" | FT
        
        value                         abbr  offset isdst text                                utc                                                              
        -----                         ----  ------ ----- ----                                ---                                                              
        West Asia Standard Time       WAST       5 False (UTC+05:00) Ashgabat, Tashkent      {Antarctica/Mawson, Asia/Aqtau, Asia/Aqtobe, Asia/Ashgabat...}   
        Central Asia Standard Time    CAST       6 False (UTC+06:00) Nur-Sultan (Astana)     {Antarctica/Vostok, Asia/Almaty, Asia/Bishkek, Asia/Qyzylorda...}
        SE Asia Standard Time         SAST       7 False (UTC+07:00) Bangkok, Hanoi, Jakarta {Antarctica/Davis, Asia/Bangkok, Asia/Hovd, Asia/Jakarta...}     
        N. Central Asia Standard Time NCAST      7 False (UTC+07:00) Novosibirsk             {Asia/Novokuznetsk, Asia/Novosibirsk, Asia/Omsk}                 
        North Asia Standard Time      NAST       8 False (UTC+08:00) Krasnoyarsk             {Asia/Krasnoyarsk}                                               
        North Asia East Standard Time NAEST      8 False (UTC+08:00) Irkutsk                 {Asia/Irkutsk}                                                   


        .EXAMPLE
        PS> Get-CommonTimeZone -Offset 10 | FT
        value                      abbr offset isdst text                                    utc                                                                           
        -----                      ---- ------ ----- ----                                    ---                                                                           
        E. Australia Standard Time EAST     10 False (UTC+10:00) Brisbane                    {Australia/Brisbane, Australia/Lindeman}                                      
        AUS Eastern Standard Time  AEST     10 False (UTC+10:00) Canberra, Melbourne, Sydney {Australia/Melbourne, Australia/Sydney}                                       
        West Pacific Standard Time WPST     10 False (UTC+10:00) Guam, Port Moresby          {Antarctica/DumontDUrville, Etc/GMT-10, Pacific/Guam, Pacific/Port_Moresby...}
        Tasmania Standard Time     TST      10 False (UTC+10:00) Hobart                      {Australia/Currie, Australia/Hobart}                                          


        .NOTES
            Name: Get-CommonTimeZone
            Version: 1.0
            Author : Himanshu Upadhye
            Date: 22 June 2022

        .NOTES
            Both paramters can not be specified at once.

    #>  
    [CmdletBinding(DefaultParameterSetName="None")]
    Param(
        [Parameter(Mandatory=$false, ParameterSetName = "Param1")]
        [ValidateNotNullOrEmpty()]
        [string] $Name = "",
        [Parameter(Mandatory=$false , ParameterSetName = "Param2")]
        [ValidateNotNullOrEmpty()]
        [ValidateRange (-12,12)]
        [string] $Offset = $null
    )

    #trying to fetch Timezone data from Git file
    Try{
        $InputFileGit = "https://raw.githubusercontent.com/dmfilipenko/timezones.json/master/timezones.json"
        $TimeZoneData = (New-Object System.Net.WebClient).DownloadString($InputFileGit) | ConvertFrom-Json -ErrorAction Stop
    }
    Catch{
        Write-Warning "Unexpected error occured while retrieving TimeZone data" ; return
    }

    #Filtering data based on provided input parameters
    IF($TimeZoneData){
            IF($Name){$OutputData = $TimeZoneData | Where{$_.value -like "*$Name*"}}
            ElseIF($Offset){$OutputData = $TimeZoneData | Where{$_.offset -eq $Offset}}
            Else{$OutputData = $TimeZoneData}
            }
    Else{ Write-Warning "Unexpected error occured while retrieving TimeZone data" ; return}
    
    #Return output
    IF($OutputData){ Write-Output $OutputData}
    Else{Write-Warning "No data found to display based on provided inputs" ;return }
    
}
